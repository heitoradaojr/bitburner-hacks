=begin
Compression I: RLE Compression
You are attempting to solve a Coding Contract. You have 10 tries remaining, after which the contract will self-destruct.


Run-length encoding (RLE) is a data compression technique which encodes data as a series of runs of a repeated single character. Runs are encoded as a length, followed by the character itself. Lengths are encoded as a single ASCII digit; runs of 10 characters or more are encoded by splitting them into multiple runs.

You are given the following input string:
    XBBBBBBBBnYYYYRRojjDDDDDDDDZZccstttttttnnnvubbbb44444kkDDDDDD
Encode it using run-length encoding with the minimum possible output length.

Examples:
    aaaaabccc            ->  5a1b3c
    aAaAaA               ->  1a1A1a1A1a1A
    111112333            ->  511233
    zzzzzzzzzzzzzzzzzzz  ->  9z9z1z  (or 9z8z2z, etc.)
=end
#require 'pry'

s = 'XBBBBBBBBnYYYYRRojjDDDDDDDDZZccstttttttnnnvubbbb44444kkDDDDDD'
rle = ''
pos = 0
# pattern = /((?<p>.)\1*)/

curr = s[pos]
nex = s[pos+1]

while pos < s.length
  if s[pos] == s[pos+1]
    count += 1
  else
    print count, s[pos]
    count = 1
  end
  pos += 1
end

#while pos < s.size
#  curr_char = s[pos]
#  if next_char == current_char
#    count += 1
#  else
#    count = 1
#  end

#  current_char = next_char
#  pos += 1
#end
#binding.pry


