#! /usr/bin/env ruby

=begin
Algorithmic Stock Trader I
You are attempting to solve a Coding Contract. You have 5 tries remaining, after which the contract will self-destruct.


You are given the following array of stock prices (which are numbers) where the i-th element represents the stock price on day i:

105,75,81,117,157,179,184,198,128,13,47,138,153,73

Determine the maximum possible profit you can earn using at most one transaction (i.e. you can only buy and sell the stock once). If no profit can be made then the answer should be 0. Note that you have to buy the stock before you can sell it
=end

require 'pry'

a = [105,75,81,117,157,179,184,198,128,13,47,138,153,73]

max = 0

for i in 0...a.size
  for j in 0...a.size
    if a[j] - a[i] > max
      max = a[j] - a[i]
    end
  end
end

puts max

# binding.pry


