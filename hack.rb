#! /usr/bin/env ruby
# HAJ

require 'json'
require 'base64'
require 'optparse'

options = {}
option_parser = OptionParser.new do |opts|
  opts.on('-f FILE') do |file|
    puts "will open #{file}"
  end
end
option_parser.parse!

#filename = ARGV[0]
#puts "ARGV.size = #{ARGV.size}"
#if ARGV.size == 0
#  puts 'Usage: ./hack.rb SAVEFILE.json'
#  exit
#end

puts options

raw = File.read(filename)
json = Base64.decode64(raw)
h = JSON.parse(json)

player_save = JSON.parse(h['data']['PlayerSave'])
# player_save = JSON.parse(player_save_json)

player_save['data']['hp'] = { "current" => 1337, "max" => 1337 }
layer_save['data']['skills'] = {
  "hacking"=>5100,
  "strength"=>30000,
  "defense"=>3000,
  "dexterity"=>3000,
  "agility"=>1000,
  "charisma"=>2000,
  "intelligence"=>0
}

player_save['data']['money'] = 999999999999999999

hacknet_nodes = player_save['data']['hacknetNodes']
hacknet_nodes.each do |node|
  #first_node = hacknet_nodes.first
  node['data']['moneyGainRatePerSecond'] = 1000000000
end

player_save['data']['achievements'] = [{"ID"=>"NS2", "unlockedOn"=>1659191951597},
 {"ID"=>"FIRST_HACKNET_NODE", "unlockedOn"=>1659209186981},
 {"ID"=>"BRUTESSH.EXE", "unlockedOn"=>1659212667006},
 {"ID"=>"INSTALL_1", "unlockedOn"=>1661310646232},
 {"ID"=>"HACKNET_NODE_10M", "unlockedOn"=>1661310646288},
 {"ID"=>"CYBERSEC", "unlockedOn"=>1661827477499},
 {"ID"=>"FTPCRACK.EXE", "unlockedOn"=>1661830117638},
 {"ID"=>"HOSPITALIZED", "unlockedOn"=>1661831797501},
 {"ID"=>"TRAVEL", "unlockedOn"=>1661832277468},
 {"ID"=>"RELAYSMTP.EXE", "unlockedOn"=>1662231775349},
 {"ID"=>"MAX_HACKNET_NODE", "unlockedOn"=>1662234289978},
 {"ID"=>"WORKOUT", "unlockedOn"=>1662245029974},
 {"ID"=>"TOR", "unlockedOn"=>1662245989979},
 {"ID"=>"DONATION", "unlockedOn"=>1662560570596},
 {"ID"=>"NITESEC", "unlockedOn"=>1662577610599},
 {"ID"=>"HTTPWORM.EXE", "unlockedOn"=>1662580850592},
 {"ID"=>"THE_BLACK_HAND", "unlockedOn"=>1662684827601},
 {"ID"=>"SQLINJECT.EXE", "unlockedOn"=>1662690648021},
 {"ID"=>"4S", "unlockedOn"=>1662856241997},
 {"ID"=>"MAX_CORES", "unlockedOn"=>1663020416013},
 {"ID"=>"FORMULAS.EXE", "unlockedOn"=>1663464816500},
 {"ID"=>"30_HACKNET_NODE", "unlockedOn"=>1663640687818},
 {"ID"=>"MONEY_1Q", "unlockedOn"=>1666105300428},
 {"ID"=>"BITRUNNERS", "unlockedOn"=>1666108060405},
 {"ID"=>"UNACHIEVABLE", "unlockedOn"=>1666108060505}]


ps_json = JSON.generate(player_save)
h['data']['PlayerSave'] = ps_json

File.write('output.json', Base64.encode64(JSON.generate(h)))

=begin
[{"level"=>50, "name"=>"NeuroFlux Governor"},
 {"level"=>1, "name"=>"Augmented Targeting I"},
 {"level"=>1, "name"=>"Augmented Targeting II"},
 {"level"=>1, "name"=>"Wired Reflexes"},
 {"level"=>1, "name"=>"Synaptic Enhancement Implant"},
 {"level"=>1, "name"=>"Neurotrainer I"},
 {"level"=>1, "name"=>"Hacknet Node CPU Architecture Neural-Upload"},
 {"level"=>1, "name"=>"Hacknet Node Cache Architecture Neural-Upload"},
 {"level"=>1, "name"=>"Hacknet Node NIC Architecture Neural-Upload"},
 {"level"=>1, "name"=>"Hacknet Node Core Direct-Neural Interface"},
 {"level"=>1, "name"=>"Hacknet Node Kernel Direct-Neural Interface"},
 {"level"=>1, "name"=>"CashRoot Starter Kit"},
 {"level"=>1, "name"=>"BitWire"},
 {"level"=>1, "name"=>"Cranial Signal Processors - Gen I"},
 {"level"=>1, "name"=>"Cranial Signal Processors - Gen II"},
 {"level"=>1, "name"=>"INFRARET Enhancement"},
 {"level"=>1, "name"=>"Speech Processor Implant"},
 {"level"=>1, "name"=>"Combat Rib I"},
 {"level"=>1, "name"=>"Artificial Synaptic Potentiation"},
 {"level"=>1, "name"=>"Neurotrainer II"},
 {"level"=>1, "name"=>"NutriGen Implant"},
 {"level"=>1, "name"=>"LuminCloaking-V1 Skin Implant"},
 {"level"=>1, "name"=>"LuminCloaking-V2 Skin Implant"},
 {"level"=>1, "name"=>"Embedded Netburner Module"},
 {"level"=>1, "name"=>"Neuralstimulator"},
 {"level"=>1, "name"=>"Cranial Signal Processors - Gen III"},
 {"level"=>1, "name"=>"Enhanced Myelin Sheathing"},
 {"level"=>1, "name"=>"The Black Hand"},
 {"level"=>1, "name"=>"DataJack"},
 {"level"=>1, "name"=>"Cranial Signal Processors - Gen IV"},
 {"level"=>1, "name"=>"Nuoptimal Nootropic Injector Implant"},
 {"level"=>1, "name"=>"Speech Enhancement"},
 {"level"=>1, "name"=>"Social Negotiation Assistant (S.N.A)"},
 {"level"=>1, "name"=>"Neural-Retention Enhancement"},
 {"level"=>1, "name"=>"SmartSonar Implant"},
 {"level"=>1, "name"=>"DermaForce Particle Barrier"},
 {"level"=>1, "name"=>"Combat Rib II"},
 {"level"=>1, "name"=>"CRTX42-AA Gene Modification"},
 {"level"=>1, "name"=>"SoA - phyzical WKS harmonizer"},
 {"level"=>1, "name"=>"Embedded Netburner Module Core Implant"},
 {"level"=>1, "name"=>"Neural Accelerator"},
 {"level"=>1, "name"=>"Neuroreceptor Management Implant"},
 {"level"=>1, "name"=>"ADR-V1 Pheromone Gene"},
 {"level"=>1, "name"=>"Nanofiber Weave"}]
=end


=begin
[{"ID"=>"NS2", "unlockedOn"=>1659191951597},
 {"ID"=>"FIRST_HACKNET_NODE", "unlockedOn"=>1659209186981},
 {"ID"=>"BRUTESSH.EXE", "unlockedOn"=>1659212667006},
 {"ID"=>"INSTALL_1", "unlockedOn"=>1661310646232},
 {"ID"=>"HACKNET_NODE_10M", "unlockedOn"=>1661310646288},
 {"ID"=>"CYBERSEC", "unlockedOn"=>1661827477499},
 {"ID"=>"FTPCRACK.EXE", "unlockedOn"=>1661830117638},
 {"ID"=>"HOSPITALIZED", "unlockedOn"=>1661831797501},
 {"ID"=>"TRAVEL", "unlockedOn"=>1661832277468},
 {"ID"=>"RELAYSMTP.EXE", "unlockedOn"=>1662231775349},
 {"ID"=>"MAX_HACKNET_NODE", "unlockedOn"=>1662234289978},
 {"ID"=>"WORKOUT", "unlockedOn"=>1662245029974},
 {"ID"=>"TOR", "unlockedOn"=>1662245989979},
 {"ID"=>"DONATION", "unlockedOn"=>1662560570596},
 {"ID"=>"NITESEC", "unlockedOn"=>1662577610599},
 {"ID"=>"HTTPWORM.EXE", "unlockedOn"=>1662580850592},
 {"ID"=>"THE_BLACK_HAND", "unlockedOn"=>1662684827601},
 {"ID"=>"SQLINJECT.EXE", "unlockedOn"=>1662690648021},
 {"ID"=>"4S", "unlockedOn"=>1662856241997},
 {"ID"=>"MAX_CORES", "unlockedOn"=>1663020416013},
 {"ID"=>"FORMULAS.EXE", "unlockedOn"=>1663464816500},
 {"ID"=>"30_HACKNET_NODE", "unlockedOn"=>1663640687818},
 {"ID"=>"MONEY_1Q", "unlockedOn"=>1666105300428},
 {"ID"=>"BITRUNNERS", "unlockedOn"=>1666108060405},
 {"ID"=>"UNACHIEVABLE", "unlockedOn"=>1666108060505}]
=end

# puts filename
# raw = File.read()


